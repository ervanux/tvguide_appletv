//
//  Channel.swift
//  TVGuide
//
//  Created by Erkan Ugurlu on 20/12/15.
//  Copyright © 2015 JokerApps. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Channel {
    var title:String?
    var iconUrl:String?
    var feeds:[ChannelFeed]!
    
    init(json:JSON){
        self.title = json[1].string
        self.iconUrl = json[0].string
        self.feeds = ChannelFeed.getArray(json[2].array)
    }
    
    static func getArray(json:[JSON]?) -> [Channel]{
        var array = [Channel]()
        if let json = json {
            for feed in json {
                let channels = Channel(json: feed)
                if let feeds = channels.feeds where !feeds.isEmpty {
                    array.append(channels)
                }
            }
        }
        return array
    }
}