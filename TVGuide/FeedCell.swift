//
//  FeedCell.swift
//  TVGuide
//
//  Created by Erkan Ugurlu on 20/12/15.
//  Copyright © 2015 JokerApps. All rights reserved.
//

import UIKit

class FeedCell : UITableViewCell{
    @IBOutlet var title:UILabel!
    @IBOutlet var time:UILabel!

    var currentFeed : Bool = false {
        didSet{
            if currentFeed {
                self.title.font = self.title.font.bold()
                self.time.font = self.time.font.bold()
                self.backgroundColor = UIColor.lightGrayColor()
            } else {
                self.title.font = UIFont.systemFontOfSize(self.title.font.pointSize)
                self.time.font = UIFont.systemFontOfSize(self.title.font.pointSize)
                self.backgroundColor = UIColor.clearColor()
            }
        }
    }
    
    var model:ChannelFeed!{
        didSet {
            self.title.text = model.title
            self.time.text = model.time
        }
    }
}
