//
//  MainViewController.swift
//  TVGuide
//
//  Created by Erkan Ugurlu on 22/11/15.
//  Copyright © 2015 JokerApps. All rights reserved.
//

import UIKit
import SwiftyJSON

enum FocusMode {
    case ChannelMode,FeedMode
}

class MainViewController: UIViewController {
    var chanelsVC:ChannelsViewController!
    var feedsVC:FeedsTableViewController!
    var indicatorVC:IndicatorViewController!
    var channels = [Channel]()
    let url = "https://tvguidecollector.appspot.com/collector?show=tv"
    
    var currentFocusMode:FocusMode = FocusMode.ChannelMode {
        didSet{
            self.setNeedsFocusUpdate()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "getContent", name: UIApplicationWillEnterForegroundNotification, object: nil)
        getContent()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func showLoading(){
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if self.indicatorVC == nil {
                self.indicatorVC = self.storyboard?.instantiateViewControllerWithIdentifier("indicatorVC") as! IndicatorViewController
            }
            self.showViewController(self.indicatorVC, sender: nil)
        })
    }
    
    func getContent(){
        showLoading()
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.cachePolicy = .ReloadIgnoringLocalCacheData
        NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if let data = data {
                let channels = Channel.getArray(JSON(data: data).array)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.chanelsVC.channels = channels
                    self.indicatorVC.dismissViewControllerAnimated(false, completion: { () -> Void in
                        
                    })
                })
            } else {
                self.showDataAlert()
            }
        }).resume()
    }
    
    func showDataAlert() {
        let alertVC = UIAlertController(title: nil, message: "Bağlantı problemi oluştu.", preferredStyle: UIAlertControllerStyle.Alert)
        self.showViewController(alertVC, sender: nil)
    }
    
    override weak var preferredFocusedView:UIView? {
        get{
            if self.chanelsVC != nil {
                switch(self.currentFocusMode) {
                case .ChannelMode:
                    return self.chanelsVC.view
                case .FeedMode:
                    return self.feedsVC.view
                }
            }else {
                return nil
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let feedVC = segue.destinationViewController as? FeedsTableViewController  {
            self.feedsVC = feedVC
        } else if let chanelsVC = segue.destinationViewController as? ChannelsViewController  {
            self.chanelsVC = chanelsVC
        }
    }
    
}

