//
//  ChannelsViewController.swift
//  TVGuide
//
//  Created by Erkan Ugurlu on 22/11/15.
//  Copyright © 2015 JokerApps. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChannelsViewController: UIViewController {

    @IBOutlet var collectionView:UICollectionView!
    var channels = [Channel](){
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let focusGuide = UIFocusGuide()
        self.collectionView.addLayoutGuide(focusGuide)
        self.collectionView.remembersLastFocusedIndexPath = true

    }
    
//    override weak var preferredFocusedView:UIView? {
//        get{
//            return self.collectionView
//        }
//    }

}

extension ChannelsViewController: UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: false)
        if let mainVC = self.parentViewController as? MainViewController {
            mainVC.currentFocusMode = FocusMode.FeedMode
        }
    }
}

//Focus
extension ChannelsViewController {
    func collectionView(collectionView: UICollectionView, canFocusItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func collectionView(collectionView: UICollectionView, didUpdateFocusInContext context: UICollectionViewFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocusInContext(context, withAnimationCoordinator: coordinator)

        if let indexPath = context.previouslyFocusedIndexPath, coll = collectionView.cellForItemAtIndexPath(indexPath) as? ChannelCell {
//            coll.image.adjustsImageWhenAncestorFocused = false
            if !coll.selected {
                let b = CATransform3DMakeScale(1.0, 1.0, 1)
                coll.image.layer.transform = b
            }
            

        }

        if let indexPath = context.nextFocusedIndexPath, coll = collectionView.cellForItemAtIndexPath(indexPath) as? ChannelCell {
//            coll.image.adjustsImageWhenAncestorFocused = false
            let b = CATransform3DMakeScale(1.3, 1.3, 1)
            coll.image.layer.transform = b
            
            if let pr = self.parentViewController as? MainViewController {
                pr.feedsVC.selectedChannel = channels[indexPath.row]
            }
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, shouldUpdateFocusInContext context: UICollectionViewFocusUpdateContext) -> Bool {
        if let _ = context.nextFocusedView as? ChannelCell {
            return true
        } else {
            return false
        }
    }
    
//    func indexPathForPreferredFocusedViewInCollectionView(collectionView: UICollectionView) -> NSIndexPath? {
//        return NSIndexPath(forRow: 0, inSection: 0)
//    }
    
//    func collectionView(collectionView: UICollectionView, targetIndexPathForMoveFromItemAtIndexPath originalIndexPath: NSIndexPath, toProposedIndexPath proposedIndexPath: NSIndexPath) -> NSIndexPath {
//        return NSIndexPath(forRow: originalIndexPath.row+1, inSection: originalIndexPath.section)
//    }
    
//    func collectionView(collectionView: UICollectionView, targetContentOffsetForProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
//    }

}

extension ChannelsViewController: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return channels.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell1", forIndexPath: indexPath) as! ChannelCell
        cell.model = channels[indexPath.row]
        
        return cell
    }
    
}


