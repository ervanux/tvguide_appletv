//
//  ChannelCell.swift
//  TVGuide
//
//  Created by Erkan Ugurlu on 20/12/15.
//  Copyright © 2015 JokerApps. All rights reserved.
//

import UIKit

class ChannelCell: UICollectionViewCell {
    @IBOutlet var image:UIImageView!
    @IBOutlet var title:UILabel!
    
    var model:Channel!{
        didSet {
            self.title.text = model.title
            if let url = model.iconUrl {
                NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: url)!, completionHandler: { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
                    if let data = data {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.image.image = UIImage(data: data)
                            self.image.layer.shadowOpacity = 0.6
                        })
                    }
                }).resume()
            }
        }
    }
    
}