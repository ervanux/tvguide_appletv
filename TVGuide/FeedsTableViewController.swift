//
//  FeedsTableViewController.swift
//  TVGuide
//
//  Created by Erkan Ugurlu on 22/11/15.
//  Copyright © 2015 JokerApps. All rights reserved.
//

import UIKit

class FeedsTableViewController: UITableViewController {
    
    private var currentIndex = 0
    
    var selectedChannel:Channel?{
        didSet{
            self.currentIndex = findCurrentFeed(selectedChannel!.feeds)
            self.tableView.reloadData()
            if self.currentIndex > -1 {
                self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: self.currentIndex, inSection: 0), atScrollPosition: UITableViewScrollPosition.Middle, animated: false)
            }
        }
    }
}

extension FeedsTableViewController {
    
    private func findCurrentFeed(feed:[ChannelFeed]?) -> Int{
        var resultIndex = 0
        if let _ = feed {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            
            let currentDate = dateFormatter.dateFromString("\(NSDate().hour()):\(NSDate().minute())")!
            if let c = self.selectedChannel, feeds = c.feeds {
                for (index, feed) in feeds.enumerate() {
                    if index+1 == feeds.count {
                        resultIndex = index
                        break
                    }
                    
                    let lastDate = dateFormatter.dateFromString(feed.time!)!
                    let nextDate = dateFormatter.dateFromString(feeds[index+1].time!)!
                    
                    if (lastDate < currentDate || lastDate == currentDate ) && (currentDate < nextDate) {
                        resultIndex = index
                        break
                    }
                }
            }
        }
        return resultIndex
    }
    
    private func strToTuple(element:ChannelFeed)->(hour:Int,min:Int){
        let dotsIndex = element.time!.rangeOfString(":")!.startIndex
        let timeHour = Int(element.time!.substringToIndex(dotsIndex))!
        let timeMin = Int(element.time!.substringFromIndex(dotsIndex))!
        
        return (hour:timeHour,min:timeMin)
        
    }
    
}

extension FeedsTableViewController {
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let selectedChannel = selectedChannel, feeds = selectedChannel.feeds {
            return feeds.count
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath) as! FeedCell

        if let selectedChannel = selectedChannel, feeds = selectedChannel.feeds {
            cell.model = feeds[indexPath.row]
            cell.currentFeed = currentIndex == indexPath.row
        }
        
        return cell
    }
    
}

//Focus
extension FeedsTableViewController {
    
    override func indexPathForPreferredFocusedViewInTableView(tableView: UITableView) -> NSIndexPath? {
        return NSIndexPath(forRow: (currentIndex == -1 ? 0 : currentIndex) , inSection: 0)
    }
    
}
