//
//  ChannelFeed.swift
//  TVGuide
//
//  Created by Erkan Ugurlu on 20/12/15.
//  Copyright © 2015 JokerApps. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ChannelFeed {
    var title:String?
    var time:String?
        
    init(json:JSON){
        self.title = json[0].string
        self.time = json[1].string
    }
    
    static func getArray(json:[JSON]?) -> [ChannelFeed]{
        var feeds = [ChannelFeed]()
        if let json = json {
            for feed in json {
                feeds.append(ChannelFeed(json: feed))
            }
        }
        return feeds
    }
}
